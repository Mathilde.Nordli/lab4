package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int columns;
    private CellState[][] grid; 

    public CellGrid(int rows, int columns, CellState initialState) {

        this.rows = rows;
        this.columns = columns;
        
        grid = new CellState[rows][columns];

        for (int row=0; row<rows; row++) {
            for (int col=0; col<columns; col++){
                grid[row][col] = initialState;
            }
        }   
        }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {

        grid[row][column] = element;


        if(row <= 0 && row > numRows()) {
                throw new IndexOutOfBoundsException("Grid need at least 1 row, you supplied "+ row); 
        }
        if(column <= 0 && column > numColumns()) {
            throw new IndexOutOfBoundsException("Grid need at least 1 column, you supplied " + column); 
        }
    }
        

    @Override
    public CellState get(int row, int column) {

        if(row <= 0 && row > numRows()) {
                throw new IndexOutOfBoundsException("Grid need at least 1 row, you supplied "+ row); }
        
        if(column <= 0 && column > numColumns()) {
            throw new IndexOutOfBoundsException("Grid need at least 1 column, you supplied " + column); }
        
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copyGrid = new CellGrid(this.rows, this.columns, CellState.DEAD);
        
        for (int row=0; row<numRows(); row++) {
            for (int col=0; col<numColumns(); col++){
                copyGrid.set(row, col, grid[row][col]);
            }
        }
        return copyGrid;
    }
    
}
